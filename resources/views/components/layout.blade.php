<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
        <title>Articles</title>
    </head>
    <body class="bg-white">
        <nav class="flex items-center justify-between flex-wrap bg-teal p-6">
            <div class="flex items-center flex-no-shrink text-white mr-6">
                <span class="font-semibold text-black text-xl tracking-tight">Gimmersta Wallpaper</span>
            </div>
            <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
                <div class="text-sm lg:flex-grow">
                    <a href="{{ route('articles') }}" class="block mt-4 lg:inline-block lg:mt-0 text-teal-lighter hover:text-white mr-4">
                        Wallpapers
                    </a>
                    <a href="#" class="block mt-4 lg:inline-block lg:mt-0 text-teal-lighter hover:text-white mr-4">
                        About
                    </a>
                    <a href="#" class="block mt-4 lg:inline-block lg:mt-0 text-teal-lighter hover:text-white">
                        Contact
                    </a>
                </div>
            </div>
        </nav>
            <div>
                {{ $slot }}
            </div>
    </body>
</html>