<x-layout>
    <div class="px-2">
        <div class="container mx-auto pt-1" >
            <!-- Image banner -->
            <div class="w-full ">
                <div class="flex flex-col md:flex-row justify-start rounded border-2">
                    <a class="w-full md:w-2/3" href="{{$article->images->first()->url}}" target="_blank">
                        <img class="w-full" id="banner" src="{{$article->images->first()->thumbnail}}" alt="{{$article->images->first()->thumbnail}}">
                    </a>
                    <!-- Description -->
                    <div class="flex items-center justify-center w-full md:w-1/3">
                        <div class="mx-auto text-center px-3">
                            <h1 class="font-sans text-black text-center uppercase text-sm pb-5 lg:text-2xl">{{$article->descriptionTitle}}</h1>
                            <h4 class="font-sans text-black uppercase text-xs pb-5">{{$article->productName}} - {{$article->price->value}} {{$article->price->currency}} / m2 </h4>
                            <h3 class="text-black text-sm lg:text-lg" id="description">
                                {{Str::of($article->descriptionText)->limit(40)}} <span onclick="readMore( {{ $article }} );" class="cursor-pointer text-blue-300 "> {{ $article->descriptionText ? 'Read more' : null }} </span>
                            </h3>
                            <div class="pt-4">
                                <button class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">Buy now!</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Thumbnails -->
                <div class="mx-auto mt-2 flex flex-row justify-start items-evenly">
                    @foreach ($article->images as $image)
                        <img class="w-1/5 mr-2" onclick="changeImage( '{{$image->thumbnail}}::{{$image->alt}}::{{$image->url}}' );" src="{{$image->thumbnail}}" alt="{{$image->alt}}" style="cursor: pointer;">
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <script>
        function changeImage(url) {
            url = url.split('::')
            const link = url[0]
            const alt = url[1]
            const fullImage = url[2]
    
            const element = document.querySelector('#banner')
            if(element) {
                element.src = link
                element.alt = alt
                updateFullScreenLink(element, fullImage)
            }
        }

        function updateFullScreenLink(element, fullImage) {
            if(element.parentNode) {
                element.parentNode.href = fullImage
            }
        }

        function readMore(article) {
            const element = document.querySelector('#description')
            element.innerHTML = article.descriptionText
        }
    </script>
</x-layout>