<x-layout>
    <div class="container mx-auto p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">
        @foreach ($articles as $article)
            <div class="rounded overflow-hidden">
                <a href="{{ route('article', [str_replace(' ', '-', $article->productName)]) }}">
                    <img class="w-full" src="{{$article->images->first()->thumbnail}}" alt="{{$article->images->first()->alt}}">
                </a>
                <div class="px-6 py-4 h-full">
                    <div class="font-bold text-xl mb-2">
                        <a href="{{ route('article', [str_replace(' ', '-', $article->productName)]) }}">
                            {{$article->productName}}
                            <h4 class="font-sans text-black uppercase text-xs pb-5">{{$article->price->value}} {{$article->price->currency}} / m2 </h4>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</x-layout>