# How to run
## Install composer dependencies
```bash
composer install
```

## .env file
```bash
cp .env.example .env
```

### Generate app key
```bash
php artisan key:generate
```

### Add DB environment variables to .env
```env
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=lavarel_demo
DB_USERNAME=sail
DB_PASSWORD=password
```

## Start

 ### Run the app
 ```bash
 ./vendor/bin/sail up
 ```

 ### Remove dangling containers / volumes if needed
 ```bash
    docker-compose down -v
```

## Migrate
### Get into the shell of the container
```bash
./vendor/bin/sail shell
```

### Run migration
```bash
php artisan migrate:fresh
```

# Visit [http://localhost/articles](http://localhost/articles)


