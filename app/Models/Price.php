<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'currency',
        'article_id'
    ];

    public function article() {
        return $this->belongsTo('App\Models\Article', 'id');
    }
}
