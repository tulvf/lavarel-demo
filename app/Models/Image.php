<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ['thumbnail', 'url', 'alt', 'article_id'];

    public function article() {
        return $this->belongsTo('App\Models\Article', 'id');
    }
}