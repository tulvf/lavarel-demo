<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'articleId',
        'productName',
        'descriptionTitle',
        'descriptionText',
    ];
    
    public function price() {
        return $this->hasOne(Price::class)->select('value', 'currency');
    }

    public function images() {
        return $this->hasMany(Image::class)->select('thumbnail', 'url', 'alt');
    }
}