<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Image;
use App\Models\Price;

class ArticlesController extends Controller
{
    public function index() {
        $response = $this->fetch();
        $this->insert($response);
        return view('articles', ['articles' => Article::all()]);
    }

    public function show($slug) {
        $title = str_replace('-', ' ', $slug);
        return view('article', [
            'article' => Article::where(
                [
                    'productName'=>$title
                ])->firstOrFail()
            ]);
    }

    // Fetch data and return simple xml element
    protected function fetch() {
        $client = new Client(['headers' => ['Accept' => 'application/xml']]);
        $url = "https://test.rebelwalls.com/articles.php";

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);
        return $this->parseResponse($response);
    }

    protected function parseResponse ($response) {
        return new \SimpleXMLElement($response->getBody()->getContents());
    }

    // Insert data into models.
    protected function insert($articles) {
        foreach ($articles as $article) {

            // Don't add duplicates && validate input.
            if(!Article::where('articleId', $article->id)->first() && $this->validateArticle($article)) {

                // Create new article
                $newArticle = Article::create([
                    'articleId' => $article->id,
                    'productName' => $article->productName,
                    'descriptionTitle' => $article->descriptionTitle,
                    'descriptionText' => $article->descriptionText,
                ]);

                // Create images
                foreach ($article->images as $images) {
                    foreach($images as $i) {
                        if($this->validateData($i)) {
                            $image = Image::create([
                                'thumbnail' => $i->thumbnail,
                                'url' => $i->url,
                                'alt' => $i->alt,
                                'article_id' => $newArticle->id
                            ]);
                            $newArticle->images()->save($image);
                        }
                    }
                }

                // Create price
                if($this->validateData($article->price)) {
                    $price = Price::create([
                        'value' => $article->price->value,
                        'currency' => $article->price->currency,
                        'article_id' => $newArticle->id
                    ]);
                    $newArticle->price()->save($price);
                }
            }
        }
    }

    // Make sure we have id and productName
    protected function validateArticle($data) {
        return !empty($data->id) && !empty($data->productName);
    }

    // Don't allow empty values
    protected function validateData($data) {
        foreach ($data as $key => $value) {
            if(empty($value)) {
                return false;
            }
        }
        return true;
    }
}
