<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article;
use App\Models\Price;
use App\Models\Image;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents(public_path('data.json'));
        $phpArray = json_decode($json, true); 

        foreach ($phpArray as $articles) {
            foreach ($articles as $article) {
                $newArticle = Article::create([
                    'articleId' => $article['id'],
                    'productName' => empty($article['productName']) ? '' : $article['productName'],
                    'descriptionTitle' => empty($article['descriptionTitle']) ? '' :  $article['descriptionTitle'],
                    'descriptionText' => empty($article['descriptionText']) ? '' : $article['descriptionText'],
                ]);

                if (!empty($newArticle->id)) {
                    foreach ($article['images'] as $images) {
                        foreach($images as $i) {
                            if(!empty($i)) {
                                $image = new Image;
                                $image->thumbnail = $i['thumbnail'];
                                $image->url = $i['url'];
                                $image->alt = $i['alt'];
                                $image->article_id = $newArticle->id;
                                $image = $newArticle->images()->save($image);
                                $this->command->info($i['thumbnail']);
                            }
                        }
                    }
                    $price = new Price;
                    $price->value = $article['price']['value'];
                    $price->currency = $article['price']['currency'];
                    $price->article_id = $newArticle->id;
                    $price = $newArticle->price()->save($price);
                }
            }
        }
    }
}
